
# http://127.0.0.1:5000/api/v1/resultado/?alumno=
import requests

def RequestGet(Url, tostring=False):
    r = requests.get(Url)
    if (tostring == True):
        content = r.content
        print(content)

if '__main__' == __name__:
    Url = 'http://127.0.0.1:5000/api/v1/resultado/?alumno=LeonardoBack'
    RequestGet(Url, True)