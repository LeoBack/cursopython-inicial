import requests
#r = requests.get('https://api.github.com/events')
#print('Status code: {0}'.format (r.status_code))
#print('Header: {0}'.format(r.headers))
#json_content = json.loads(r.content)
#content = r.content
#print(content)

#import requests
#r = requests.post('https://api.github.com/events')
#print('Status code: {0}'.format (r.status_code))
#print('Header: {0}'.format(r.headers))


def RequestGet(Url, tostring=False):
    r = requests.get(Url)
    if (tostring == True):
        print('RequestGet')
        print('Status code: {0}'.format (r.status_code))
        print('Header: {0}'.format(r.headers))

def RequestPost(Url, tostring=False):
    r = requests.post(Url)
    if (tostring == True):
        print('RequestPost')
        print('Status code: {0}'.format (r.status_code))
        print('Header: {0}'.format(r.headers))

def Request(Url, tostring=False, metod='post'):
    if (metod.lower() == 'post'):
        RequestPost(Url, tostring)
    else:
        RequestGet(Url, tostring)

if '__main__' == __name__:
    Url = 'https://api.github.com/events'
    Request(Url, True)
    print('='*30)
    Request(Url, True, 'get')
    