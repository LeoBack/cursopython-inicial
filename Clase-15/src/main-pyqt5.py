import sys

from PyQt5 import QtWidgets

from mainwindow import Ui_MainWindow

class mywindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.btnClicked)

    def btnClicked(self):
        test1 = self.ui.lineEdit.text()
        test2 = self.ui.lineEdit_2.text()
        self.ui.label_3.setText('=> {0} - {1}'.format(test1, test2))


app = QtWidgets.QApplication([])
application = mywindow()
application.show()
sys.exit(app.exec())
