import sys

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = QWidget()
    w.setWindowTitle('Ventana PyQT-5')
    btn = QPushButton('Este es un Button', w)
    btn.setToolTip('This is a <b>QPushButton</b> widget')
    btn.move(150, 50)
    w.show()
    sys.exit(app.exec_())

