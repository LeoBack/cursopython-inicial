lista = [1,2,3,4,5,6,7,8,9,"texto", [1,'a']]
resulta = ''

''' Listas '''
lista = [1,2,3,4,5,6,7,8,9,"texto", [1,'a']]
for vlist in lista:
    print(vlist)

''' Listas. Otra manera de acomodar OK'''
lista = [1,2,3,4,5,6,7,8,9,"texto", [1,'a']]
resulta = ''

for vlist in lista:
    resulta += '{0} '.format(vlist)
print(resulta)


''' Listas. Otra manera de acomodar OK'''
lista = [1,2,3,4,5,6,7,8,9,"texto", [1,'a']]
resulta = ''

for index in range(len(lista)):
    resulta = 'Indice: {0}, Valor: {1}'.format(index, lista[index])
    print(resulta)

''' Lista. Otra manera de acomodar OK'''
lista = [1,2,3,4,5,6,7,8,9,"texto", [1,'a']]
resulta = ''

for index in range(len(lista)):
    resulta = 'Indice: {0}, Valor[{0}]: {1}'.format(index, lista[index])
    print(resulta)

''' Ejemplos listas string '''
resultadov = '123.ert3456.9yt,0943'
print(resultadov.replace('.', '-'))
print(resultadov.upper())

''' Ejemplos listas string '''
resultadov = '  sdg78sdgs  sdgsdg8976s     dg45   g4.56'
while '  ' in resultadov:
    resultadov = resultadov.replace('  ', ' ') 
    print(resultadov)

''' Listas. Repalce '''
resultadov = '  sdg78sdgs-  sdgsdg8976s     dg45   g4.56'
replace = { '    ': '-', '-': '_', '_':' ' }
for key in replace.keys():
    while key in resultadov:
        resultadov = resultadov.replace(key, replace[key])
        print(resultadov)

''' Listas. extends '''
numeros = [2,3,4,5,6]
caracteres = ['A','B','C','D']
numeros.extend(caracteres)
print('numeros extend: {0}'.format(numeros))

numeros = [2,3,4,5,6]
caracteres = ['A','B','C','D']
print ('caracteres + numeros: {0}'.format(caracteres + numeros))

''' Listas. Remove '''
numeros = [2,3,4,5,6,3]
numeros = numeros.remove(3)
print('Remove: {0}'.format(numeros))

''' Listas. index. trae solo la posicion de la primera concidencia '''
numeros = [2,3,4,5,6,3]
print('numeros index 3: {0}'.format(numeros.index(3)))

''' Listas. Pop. Cola'''
numeros = [2,3,4,5,6,3]
for i in range(3):
    numeros.pop()
    print('numeros pop: {0}, R: {1}'.format(i, numeros))

print('numeros: {0}'.format(numeros))

''' Listas. Pop index. Cola'''
numeros = [2,3,4,5,6,3]
for i in range(3):
    numeros.pop(2)
    print('numeros pop: {0}, R: {1}'.format(i, numeros))

print('numeros: {0}'.format(numeros))


''' Listas. Sort, Reverce. Ordenan lista '''
numeros = [2,3,4,5,6,3, 'a', 'c', 'b']
numeros.sort()
print(numeros)
numeros.reverse()
print(numeros)

''' Diccionarios '''
diccionario = { 'nombre': 'Leo', 'Apellido': 'Back', 'edad':30  }

''' Traer un valor '''
print(diccionario['nombre'])

diccionario = { 
    'nombre': 'Leo', 'Apellido': 'Back', 'edad':30,
    'nombre': 'A', 'Apellido': '1', 'edad':11
    'nombre': 'B', 'Apellido': '2', 'edad':12
    'nombre': 'C', 'Apellido': '3', 'edad':13  }


dic = dict(zip('abcd', [1,2,3,4])) 
    { 'a':1, 'b':2, 'c':3, 'd':4 }

''' llaves del diccionario'''
dic = { 'a':1, 'b':2, 'c':3, 'd':4 }
print(dic.keys())

''' lista desde un diccionario '''
Items = dic.items()
print(Items)

''' diccionario. Sort,  Ordenan lista '''
dic = { 'a':1, 'b':2, 'c':3, 'd':4 }
k = dic.keys()
K.sort()
print(k)
'''k.reverse()
print(k)'''

''' Diccionario. Error '''
diccionario = { 
    'nombre': 'Leo', 'Apellido': 'Back', 'edad':30,
    'nombre': 'A', 'Apellido': '1', 'edad':11
    'nombre': 'B', 'Apellido': '2', 'edad':12
    'nombre': 'C', 'Apellido': '3', 'edad':13  }
diccionario = ['keyA']

''' Diccionario. para tratar estos errores '''
dic = { 'a': 1, 'b': 2, 'c':3, 'd':4}
print('Existe: {0}'.format(dic.get('b')))
print('Existe: {0}'.format(dic.get('keyA')))
print('Existe: {0}'.format(dic.get('keyA', 'n/a')))

''' Diccionario. Pop '''

''' Diccionario. update '''
dic1 = { 'a': 1, 'b': 2, 'c':3, 'd':4}
dic2 = { 'c': 5, 'd': 6, 'e':7, 'f':8}
dic1.update(dic2)
print(dic1)



