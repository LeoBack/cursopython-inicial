# Tarea #1:
# Debes crear una funcion funcion que por medio de request apuntado a la url que se detalla a continuacion
#  y alli se recibira la instruccion del proximo paso dentro del content.
# URL:
# https://4etjz3ty75.execute-api.us-east-1.amazonaws.com/default/curso_step_2?id=7707766712006331404

import requests

def request_get(webpage, tostring=False):
    r = requests.get(webpage)
    content = r.content
    print('Content:\n{0}'.format(content))
    if (tostring == True):
        print('Request Get')
        print('Status code: {0}'.format (r.status_code))
        print('Header:\n{0}'.format(r.headers))


def request_post(Url, tostring=False):
    r = requests.post(Url)
    content = r.content
    print('Content:\n{0}'.format(content))
    if (tostring == True):
        print('Request Post')
        print('Status code: {0}'.format (r.status_code))
        print('Header:\n{0}'.format(r.headers))

def request(Url, tostring=False, metod='post'):
    if (metod.lower() == 'post'):
        request_post(Url, tostring)
    else:
        request_get(Url, tostring)

def run_request():
    Url = 'https://4etjz3ty75.execute-api.us-east-1.amazonaws.com/default/curso_step_2?id=7707766712006331404'
    print('='*20)    
    print('Request URL: {0}'.format(Url))
    print('='*20)    
    request(Url, True)
    print('='*20)
    request(Url, True, 'get')
    print('='*20)
    print('End')

if '__main__' == __name__:
    run_request()