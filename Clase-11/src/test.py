# trabajamos con archivos (Texto plano)
from funciones import *

def user_control():
    repeat = True
    while (repeat):
        command = -1
        while (command < 0 or command > 6):
            print('{0}\tGenerar archivo (while)'.format(1))
            print('{0}\tEscribir archivo metodo 1 (write)'.format(2))
            print('{0}\tLeer archivo metodo 1 (readline/s)'.format(3))
            print('{0}\tLeer archivo metodo 2 (for)'.format(4))
            print('{0}\tCopiar archivo metodo 1 (for)'.format(5))
            print('{0}\tCopiar archivo metodo 2 (write-read)'.format(6))
            print('{0}\tSalir'.format(0))
            command = int(input('=> ingrese numero de la opcion: '))
            print('Opcion Seleccionada: {0}'.format(command))
            

        if (command == 0):
            repeat = False
            print('Aplicacion finalizada')
        elif (command == 1):
            namefile = input("Nombre del archivo: ")
            test(namefile)
        elif (command == 2):
            clase_write()
        elif (command == 3):
            clase_read()
        elif (command == 4):
            clase_readfor()
        elif (command == 3):
            namefile = input("Nombre del archivo: ")
            copy_text(namefile)
        else:
            namefile = input("Nombre del archivo: ")
            copy_text2(namefile)

    if (repeat == False):
        exit()

user_control()